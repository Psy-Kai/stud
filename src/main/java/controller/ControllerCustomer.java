package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerCustomer implements Initializable, ControllerWithModelShop {
    @FXML
    private ListView<other.Product> listViewProducts;
    @FXML
    private TableView<other.Order> tableViewOrder;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        listViewProducts.setCellFactory(
                param -> {
                    return new ListViewProductsCell();
                }
        );
    }

    static class ListViewProductsCell extends ListCell<other.Product> {
        @Override
        public void updateItem(other.Product item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setText(item.getName());
            }
        }
    }

    @FXML
    protected void handleAddButtonAction(ActionEvent event) {

    }

    @Override
    public void setModelShop(model.ModelShop<other.Product> model) {
        listViewProducts.setItems(model);
    }
}
