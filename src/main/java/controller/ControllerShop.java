package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import fpt.com.SerializableStrategy;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import other.IDGenerator;
import other.serializingstrategies.BinaryStrategy;
import other.serializingstrategies.XMLStrategy;
import other.serializingstrategies.XStreamStrategy;

public class ControllerShop implements Initializable, ControllerWithModelShop {
	@FXML
	private TableView<other.Product> tableView;
	@FXML
	private TextField textFieldName;
	@FXML
	private TextField textFieldPrice;
	@FXML
	private TextField textFieldQuantity;
	@FXML
	private ComboBox<SerializableStrategy> comboBoxStrategy;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tableView.getSelectionModel().selectedItemProperty().addListener(
				(obs, oldSelection, newSelection) -> {
					other.Product selection = null;

					if (newSelection != null) {
						selection = newSelection;
					}
					else if ((oldSelection != null) && (!tableView.getItems().isEmpty())) {
						selection = oldSelection;
					}

					if (selection != null) {
						textFieldName.setText(newSelection.getName());
						textFieldPrice.setText(Double.toString(newSelection.getPrice()));
						textFieldQuantity.setText(Integer.toString(newSelection.getQuantity()));
					}
				});

		comboBoxStrategy.setCellFactory(
				param -> {
					return new ComboBoxStrategyCell();
				}
		);

		comboBoxStrategy.getItems().add(new BinaryStrategy());
		comboBoxStrategy.getItems().add(new XMLStrategy());
		comboBoxStrategy.getItems().add(new XStreamStrategy());
		comboBoxStrategy.getSelectionModel().select(0);
	}

	static class ComboBoxStrategyCell extends ListCell<SerializableStrategy> {
		@Override
		public void updateItem(SerializableStrategy item, boolean empty) {
			super.updateItem(item, empty);
			if (item != null) {
				setText(item.getClass().getName());
			}
		}
	}

	@FXML
	protected void handleLoadButtonAction(ActionEvent event) {
		SerializableStrategy serial = comboBoxStrategy.getSelectionModel().getSelectedItem();
		try {
			serial.open("products.ser");
			fpt.com.Product obj = serial.readObject();
			while (obj != null) {
				obj.setId(IDGenerator.singleton().getNewId());
				tableView.getItems().add((other.Product) obj);
				obj = serial.readObject();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (other.exceptions.IDOverflow e) {
			e.printStackTrace();
		}
	}

	@FXML
	protected void handleSaveButtonAction(ActionEvent event) {
		SerializableStrategy serial = comboBoxStrategy.getSelectionModel().getSelectedItem();
		try {
			serial.open("products.ser");
			for (other.Product obj: tableView.getItems()) {
				serial.writeObject(obj);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	protected void handleAddButtonAction(ActionEvent event) {
		other.Product p = new other.Product();
		try {
			p.setId(other.IDGenerator.singleton().getNewId());
			if (updateProduct(p))
				tableView.getItems().add(p);
		}
		catch (other.exceptions.IDOverflow e) {
		}

		clearFields();
	}

	@FXML
	protected void handleRemoveButtonAction(ActionEvent event) {
		other.Product product = tableView.getSelectionModel().getSelectedItem();
		if (product != null) {
			tableView.getItems().remove(product);
		}


		clearFields();
	}

	@FXML
	protected void handleUpdateButtonAction(ActionEvent event) {
		other.Product product = tableView.getSelectionModel().getSelectedItem();
		if (product != null) {
			updateProduct(product);
		}
	}

	private void clearFields() {
		textFieldName.setText("");
		textFieldPrice.setText("");
		textFieldQuantity.setText("");
	}

	private boolean updateProduct(other.Product product) {
		if (textFieldName.getText().isEmpty() ||
				textFieldPrice.getText().isEmpty() ||
				textFieldQuantity.getText().isEmpty())
			return false;

		product.setName(textFieldName.getText());

		try {
			product.setPrice(Double.parseDouble(textFieldPrice.getText()));
			product.setQuantity(Integer.parseInt(textFieldQuantity.getText()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public void setModelShop(model.ModelShop<other.Product> model) {
		tableView.setItems(model);
	}
}
