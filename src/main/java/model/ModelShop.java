package model;

import javafx.collections.ModifiableObservableListBase;
import java.util.ArrayList;
import java.util.List;

public class ModelShop<T> extends ModifiableObservableListBase<T> {
	private final List<T> delegate = new ArrayList<T>();
	
	@Override
	protected void doAdd(int index, T element) {
		delegate.add(index, element);
	}

	@Override
	protected T doRemove(int index) {
		return delegate.remove(index);
	}

	@Override
	protected T doSet(int index, T element) {
		return delegate.set(index, element);
	}

	@Override
	public T get(int index) {
		return delegate.get(index);
	}

	@Override
	public int size() {
		return delegate.size();
	}
}
