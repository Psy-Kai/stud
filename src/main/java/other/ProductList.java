package other;

import java.util.ArrayList;
import fpt.com.Product;

@SuppressWarnings("serial")
public class ProductList extends ArrayList<Product> implements fpt.com.ProductList {
	/* this class is not used because ModelShop would not work with a TableView if we use this
	 * list class */


	@Override
	public Product findProductById(long id) {
		Product product = null;
		while (iterator().hasNext() && ((product = iterator().next()).getId() != id));
		return product;
	}

	@Override
	public Product findProductByName(String name) {
		Product product = null;
		while (iterator().hasNext() && ((product = iterator().next()).getName() != name));
		return product;
	}

	@Override
	public boolean delete(Product product) {
		return remove(product);
	}
}
