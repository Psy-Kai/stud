package other.serializingstrategies;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;

public class LongPropertyConverter implements SingleValueConverter {
    @Override
    public String toString(Object o) {
        if (o == null)
            return null;

        return ((LongProperty)o).getValue().toString();
    }

    @Override
    public Object fromString(String s) {
        try {
            return new SimpleLongProperty(Long.parseLong(s));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass == SimpleLongProperty.class;
    }
}
