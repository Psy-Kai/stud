package other.serializingstrategies;

import com.thoughtworks.xstream.XStream;
import fpt.com.Product;

import java.io.*;

public class XStreamStrategy implements fpt.com.SerializableStrategy {
    XStream xstream = createXStream(other.Product.class);
    InputStream inputStream = null;
    OutputStream outputStream = null;

    public XStreamStrategy() {
        xstream.registerLocalConverter(other.Product.class, "name", new StringPropertyConverter());
        xstream.registerLocalConverter(other.Product.class, "price", new DoublePropertyConverter());
        xstream.registerLocalConverter(other.Product.class, "quantity", new IntegerPropertyConverter());
//        xstream.registerConverter(new IntegerPropertyConverter());
//        xstream.registerConverter(new DoublePropertyConverter());
//        xstream.registerConverter(new LongPropertyConverter());
    }

    @Override
    public Product readObject() throws IOException {
        if (inputStream == null)
            return null;

        return (Product) xstream.fromXML(inputStream);
    }

    @Override
    public void writeObject(Product obj) throws IOException {
        if (outputStream != null) {
            xstream.toXML(obj, outputStream);
        }
    }

    @Override
    public void close() throws IOException {
        if (inputStream != null)
            inputStream.close();
        if (outputStream != null)
            outputStream.close();
    }

    @Override
    public void open(InputStream input, OutputStream output) throws IOException {
        if (input == null)
            inputStream = null;
        else
            inputStream = input;

        if (output == null)
            outputStream = null;
        else
            outputStream = output;
    }
}
