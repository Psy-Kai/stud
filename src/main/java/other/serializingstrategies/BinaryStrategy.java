package other.serializingstrategies;

import fpt.com.Product;

import java.io.*;

public class BinaryStrategy implements fpt.com.SerializableStrategy {
    private ObjectInputStream inputStream = null;
    private ObjectOutputStream outputStream = null;

    @Override
    public Product readObject() throws IOException {
        if (inputStream == null)
            return null;

        try {
            other.Product obj = new other.Product();
            obj.readExternal(inputStream);
            return obj;
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void writeObject(Product obj) throws IOException {
        if (outputStream != null) {
            ((other.Product)obj).writeExternal(outputStream);
            outputStream.flush();
        }
    }

    @Override
    public void close() throws IOException {
        if (inputStream != null)
            inputStream.close();
        if (outputStream != null)
            outputStream.close();
    }

    @Override
    public void open(InputStream input, OutputStream output) throws IOException {
        if (input == null)
            inputStream = null;
        else
            inputStream = new ObjectInputStream(input);

        if (output == null)
            outputStream = null;
        else
            outputStream = new ObjectOutputStream(output);
    }
}
