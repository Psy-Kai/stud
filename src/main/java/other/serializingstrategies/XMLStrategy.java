package other.serializingstrategies;

import fpt.com.Product;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;


public class XMLStrategy implements fpt.com.SerializableStrategy {
    XMLDecoder inputStream;
    XMLEncoder outputStream;

    @Override
    public Product readObject() throws IOException {
        if (inputStream == null)
            return null;

        return (other.Product)inputStream.readObject();
    }

    @Override
    public void writeObject(Product obj) throws IOException {
        if (outputStream != null) {
            outputStream.writeObject(obj);
            outputStream.flush();
        }
    }

    @Override
    public void close() throws IOException {
        if (inputStream != null)
            inputStream.close();
        if (outputStream != null)
            outputStream.close();
    }

    @Override
    public void open(InputStream input, OutputStream output) throws IOException {
        if (input == null)
            inputStream = null;
        else
            inputStream = new XMLDecoder(input);

        if (output == null)
            outputStream = null;
        else
            outputStream = new XMLEncoder(output);
    }
}
