package other.serializingstrategies;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class IntegerPropertyConverter implements SingleValueConverter {
    @Override
    public String toString(Object o) {
        if (o == null)
            return null;

        return ((IntegerProperty)o).getValue().toString();
    }

    @Override
    public Object fromString(String s) {
        try {
            return new SimpleIntegerProperty(Integer.parseInt(s));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass == SimpleIntegerProperty.class;
    }
}
