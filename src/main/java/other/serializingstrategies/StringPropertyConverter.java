package other.serializingstrategies;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StringPropertyConverter implements SingleValueConverter {
    @Override
    public String toString(Object o) {
        if (o == null)
            return null;

        return ((StringProperty)o).getValue();
    }

    @Override
    public Object fromString(String s) {
        return new SimpleStringProperty(s);
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass == SimpleStringProperty.class;
    }
}
