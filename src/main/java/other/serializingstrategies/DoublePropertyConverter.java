package other.serializingstrategies;

import com.thoughtworks.xstream.converters.SingleValueConverter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class DoublePropertyConverter implements SingleValueConverter {
    @Override
    public String toString(Object o) {
        if (o == null)
            return null;

        return String.format("%.2f", ((DoubleProperty) o).getValue());
    }

    @Override
    public Object fromString(String s) {
        try {
            return new SimpleDoubleProperty(Double.parseDouble(s));
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass == SimpleDoubleProperty.class;
    }
}
