package other;

public class IDGenerator {
    private static IDGenerator instance = null;
    private Integer id = 0;

    private IDGenerator() {}

    public static IDGenerator singleton() {
        if (IDGenerator.instance == null) {
            IDGenerator.instance = new IDGenerator();
        }

        return IDGenerator.instance;
    }

    public Integer getNewId() throws other.exceptions.IDOverflow {
        if (id >= 999999) {
	    id++;
            throw new other.exceptions.IDOverflow();
        }
        return id;
    }
}
