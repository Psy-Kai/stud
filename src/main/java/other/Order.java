package other;

import java.util.ArrayList;
import fpt.com.Product;

@SuppressWarnings("serial")
public class Order extends ArrayList<Product> implements fpt.com.Order {	
	@Override
	public Product findProductById(long id) {
		Product product = null;
		while (iterator().hasNext() && ((product = iterator().next()).getId() != id));
		return product;
	}

	@Override
	public Product findProductByName(String name) {
		Product product = null;
		while (iterator().hasNext() && ((product = iterator().next()).getName() != name));
		return product;
	}

	@Override
	public double getSum() {
		double sum = 0;
		while (iterator().hasNext())
		{
			sum += iterator().next().getPrice();
		}
		
		return sum;
	}

	@Override
	public int getQuantity() {
		return size();
	}

	@Override
	public boolean delete(Product p) {
		return remove(p);
	}
}
