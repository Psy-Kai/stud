package other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

@XStreamAlias("ware")
public class Product implements fpt.com.Product, java.io.Externalizable {
	private StringProperty name = new SimpleStringProperty();
	/* do not save id
	 * id should be unique so do not serialize id, let it assign from a value from IDGenerator!
	 */
	@XStreamOmitField
	private LongProperty id = new SimpleLongProperty();
	@XStreamAlias("preis")
	private DoubleProperty price = new SimpleDoubleProperty();
	@XStreamAlias("anzahl")
	private IntegerProperty quantity = new SimpleIntegerProperty();
	
	@Override
	public long getId() {
		return id.get();
	}

	@Override
	public void setId(long id) {
		this.id.set(id);
	}

	@Override
	public double getPrice() {
		return price.get();
	}

	@Override
	public void setPrice(double price) {
		this.price.set(price);

	}

	@Override
	public int getQuantity() {
		return quantity.get();
	}

	@Override
	public void setQuantity(int quantity) {
		this.quantity.set(quantity);
	}

	@Override
	public String getName() {
		return name.get();
	}

	@Override
	public void setName(String name) {
		this.name.set(name);
	}

	@Override
	public ObservableValue<String> nameProperty() {
		return name;
	}

	@Override
	public ObservableValue<Number> priceProperty() {
		return price;
	}

	@Override
	public ObservableValue<Number> quantityProperty() {
		return quantity;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeUTF(name.getValue());
		out.writeDouble(price.getValue());
		out.writeInt(quantity.getValue());
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		name.set(in.readUTF());
		price.set(in.readDouble());
		quantity.set(in.readInt());
	}
}
