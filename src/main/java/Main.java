import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {	
	public static void main(String[] args) {
		Application.launch(args);
	}	
	
	@Override
	public void start(Stage stage) throws Exception {			
		try {
			/* create model */
			model.ModelShop<other.Product> model = new model.ModelShop<>();

			FXMLLoader loader = null;
			Scene scene = null;
			/* * * setup stage * * */
			/* load view and pass it to a scene */
			loader = new FXMLLoader(getClass().getResource("/view/ViewShop.fxml"));
			scene = new Scene(loader.load());

			/* pass model to controller */
			loader.<controller.ControllerShop>getController().setModelShop(model);

			/* pass scene to stage and show */
			stage.setScene(scene);
			stage.show();

			/* same for other stages */
			loader = new FXMLLoader(getClass().getResource("/view/ViewCustomer.fxml"));
			scene = new Scene(loader.load());
			loader.<controller.ControllerCustomer>getController().setModelShop(model);
			Stage customerStage = new Stage();
			customerStage.setScene(scene);
			customerStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}